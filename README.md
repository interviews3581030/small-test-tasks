# [small test tasks](https://gitlab.com/interviews3581030/small-test-tasks)

## Small test tasks

- Write a Docker file for nodeJS application ( https://github.com/heroku/node-js-getting-started ). Make sure to use all the Best Practices for Docker, it will be evaluated.
- Prepare a docker-compose with Loki, Grafana, Promtail and nodeJS application ( https://github.com/heroku/node-js-getting-started ). Setup log collection from nodeJS application.
- Write a simple script (in any language) that will print the numbers from 0 to 100 and convert every tenth to a wordy version.

## Answers

- [Write a Docker file for nodeJS application ( https://github.com/heroku/node-js-getting-started ). Make sure to use all the Best Practices for Docker, it will be evaluated](01/README.md)

- [Prepare a docker-compose with Loki, Grafana, Promtail and nodeJS application ( https://github.com/heroku/node-js-getting-started ). Setup log collection from nodeJS application.](02/README.md)

- [Write a simple script (in any language) that will print the numbers from 0 to 100 and convert every tenth to a wordy version.](03/README.md)

